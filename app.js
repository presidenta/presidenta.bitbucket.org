angular.module('dilmaApp', [])
    .controller('Controller', function() {
        var dilma = this;
        
        var Prefix = function(arg1, arg2) {
            if (arg2 === undefined) {
                var otherPrefix = arg1;
                this.pref = otherPrefix.pref.slice();
            } else {
                var n = arg1;
                var str = arg2;
                this.pref = [];
                for (var i=0; i<n; i++) {
                    this.pref.push(str);
                }
            }
        };

        Prefix.prototype.as_key = function() {
            return this.pref.join();
        };

        var Chain = function() {
            this.npref = 2;
            this.noword = "\n";
            this.statetab = {}; // key = prefix, value = suffix array
            this.prefix = new Prefix(this.npref, this.noword);
        };

        Chain.prototype.build = function(treino) {
            var words = treino.split(/\s+/);
            for (var i=0; i<words.length; i++) {
                this.add(words[i]);
            }
            this.add(this.noword);
        };

        Chain.prototype.add = function(word) {
            var suf = this.statetab[this.prefix.as_key()];
            if (suf === undefined) {
                suf = [];
                this.statetab[new Prefix(this.prefix).as_key()] = suf;
            }
            suf.push(word);
            this.prefix.pref.shift();
            this.prefix.pref.push(word);
        }

        Chain.prototype.generate = function(minwords) {
            var output = [];
            this.prefix = new Prefix(this.npref, this.noword);
            while (output.length < minwords || !output[output.length-1].endsWith('.')) {
                var suffixes = this.statetab[this.prefix.as_key()];
                var suffix = suffixes[Math.floor(Math.random() * suffixes.length)];
                if (suffix === this.noword) {
                    break;
                }
                output.push(suffix);
                this.prefix.pref.shift();
                this.prefix.pref.push(suffix);
            }

            return output.join(' ');
        };

        dilma.geraDiscurso = function() {
            var minwords = 50;
            dilma.generate(minwords);
        };

        dilma.generate = function(minwords) {
            var chain = new Chain();
            shuffle(dilma.treino);
            chain.build(dilma.treino.join(' '));
            dilma.discurso = chain.generate(minwords);
        };

        var shuffle = function(array) {
            var currentIndex = array.length, temporaryValue, randomIndex;
            while (0 !== currentIndex) {
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }
            return array;
        };

        dilma.treino = ["Se hoje é o Dia das Crianças, ontem eu disse que criança… o dia da criança é dia da mãe, do pai e das professoras, mas também é o dia dos animais. Sempre que você olha uma criança, há sempre uma figura oculta, que é um cachorro atrás, o que é algo muito importante. ",
            "Foi muito, houve uma procura imensa, tinham 6 empresas que apresentaram suas propostas, houve um deságio de quase... foi um pouco mais de 38%, mas eu fico em 38% para ninguém dizer: “Ah, ela disse que era 38”, mas não é não. É 39, 38 e qualquer coisa ou é 36. 38, eu acho que é 39, mas vou dizer 38." ,
            "Então, o prefeito recebia um SMS, porque a gente passava o dinheiro para o prefeito, o prefeito vai construindo a creche e vai demonstrando que está construindo. A gente manda o SMS e ele manda uma fotografia, do SMS - é o cachorro, sim. Nós descobrimos que estava errado, em um determinado caso, porque o cachorro era o mesmo em quatro creches." ,
            "A mesma coisa foi quando eu estava monitorando um programa de hospitais, eles estavam me apresentando um programa de hospitais, e tinha um balde no corredor do hospital. Perguntado, por telefone, porque o balde estava lá, a resposta foi que estava chovendo. Só que nós estávamos na mesma cidade e o sol raiava, certo? É assim que a vida é. " ,
            "Você sabe porque que eu puxo, não é? Deixa eu explicar para eles porque eu estou devendo há 100 anos - há 100 anos... Não, e eu prometi. O pior é que eu prometi. Está bom é só por isso, a Tânia tem essa força sobre o fato de eu não ter cumprido ainda a promessa. Vai, Tânia. Agora, a Tânia não vai querer que eu saia do meu regime, que eu faça um bacalhau ou uma massa, não é, Tânia? Não, vai... Pode falar, Tânia. " ,
            "Obviamente, com uma postura, de um lado, uma postura humilde, porque você tem, para você dialogar, você tem de aceitar o diálogo." ,
            "Por que hoje? Porque era o dia que eu podia e ele podia. Eu podia, quase que eu não podia, tá?, porque eu vim…, eu vinha pra cá, mas como tem duas horas de fuso, ou seja, eu posso sair meio-dia de Brasília e chegar às 10h eu fiz a reunião com o ministro Toffoli por quê? Porque aí eu vou contar uma coisa para vocês: o ministro Toffoli e nós temos um interesse em comum que é o cadastramento e a identificação de cada um de nós com um documento. " ,
            "Eu acredito que as mulheres... Elas crescentemente vão... Bom, se nós somos parte do céu como dizem os chineses, agora deu, nós queremos ser metade do céu, segundo os chineses. Deu para eles, nós queremos ser é metade do mundo mesmo e, aí, isso se expressa também em presidentes e presidentas, tá? E não sejam preconceituosos contra as mulheres. " ,
            "Paes é o prefeito mais feliz do mundo, que dirige a cidade mais importante do mundo e da galáxia. Por que da galáxia? Porque a galáxia é o Rio de Janeiro. A via láctea é fichinha perto da galáxia que o nosso querido Eduardo Paes tem a honra de ser prefeito. " ,
            "Nenhuma civilização nasceu sem ter acesso a uma forma básica de alimentação e aqui nós temos uma, como também os índios e os indígenas americanos têm a deles. Temos a mandioca e aqui nós estamos e, certamente, nós teremos uma série de outros produtos que foram essenciais para o desenvolvimento de toda a civilização humana ao longo dos séculos. Então, aqui, hoje, eu tô saudando a mandioca, uma das maiores conquistas do Brasil. " ,
            "Aqui tem uma bola, uma bola que eu acho que é um exemplo. Ela é extremamente leve, já testei aqui, testei embaixadinha, meia embaixadinha... Bom, mas a importância da bola é justamente essa, é símbolo da capacidade que nos distingue. " ,
            "Nós somos do gênero humano, da espécie sapiens, somos aqueles que têm a capacidade de jogar, de brincar, porque jogar é isso aqui. O importante não é ganhar e sim celebrar. Isso que é a capacidade humana, lúdica, de ter uma atividade cujo o fim é ele mesmo, a própria atividade. Esporte tem essa condição, essa benção, ele é um fim em si. E é essa atividade que caracteriza primeiro as crianças, a atividade lúdica de brincar. Então, para mim, essa bola é o simbolo da nossa evolução, quando nós criamos uma bola dessas, nos transformamos em Homo sapiens ou mulheres sapiens. " ,
            "Nós não vamos colocar uma meta. Nós vamos deixar uma meta aberta. Quando a gente atingir a meta, nós dobramos a meta. "];
    });
